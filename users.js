// users.js
// Fake list of users to be used in the authentication
var users = [
    {id: 1, name: "Paulo", email: "paulo10.1977@yahoo.com.br", password: "1234"},
    {id: 2, name: "Sarah", email: "sarah@mail.com", password: "sarah123"}
];
    
module.exports = users;