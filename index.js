// index.js
const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const users = require("./users")
const auth = require("./auth.js")();
//const jwt = require("jwt-simple");
const cfg = require("./config")
const session = require('express-session')
const jwt = require('jsonwebtoken');

app.use(bodyParser.json());
app.use(auth.initialize());

app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
    cookie: { 
        secure: true,
        maxAge  : new Date(Date.now() + (120 * 1000 * 30)) // 120 minutes
    }
}))


app.use(auth.session());

app.get("/", function(req, res) {
  res.json({status: "My API is alive!"});
});

// use the passaport middleware
app.get("/user", auth.authenticate(), function(req, res) {
    console.log(req.user)

    res.json(users[req.user.id]);
});
  

app.get('/logout', function(req, res){
    req.logout();
    //res.redirect('/');
    console.log('session', req.session)
    req.session.destroy(function (err) {
        if (err) { return next(err); }
        // The response should indicate that the user is no longer authenticated.
        return res.send({ authenticated: req.isAuthenticated() });
    });
})

// auth
app.post("/login", function(req, res) {
    if (req.body.email && req.body.password) {
        var email = req.body.email;
        var password = req.body.password;
        var user = users.find(function(u) {
            return u.email === email && u.password === password;
        });
        if (user) {
            //var payload = {id: user.id};
            // expires in 2 minutes
            var token = jwt.sign(user, cfg.jwtSecret, { expiresIn: '120s' });
            res.json({message: 'success', token});
        } else {
            res.sendStatus(401);
        }
    } else {
        res.sendStatus(401);
    }
});

app.listen(3000, function() {
  console.log("My API is running...");
});
module.exports = app;